﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AccountSystem : MonoBehaviour {

    public static AccountSystem s_Instance = null;

    public MoneyCounter[] m_aryGreenCash;

    public int m_nGreenCash = 100000; // 绿票。 绿票是绑定账号的，金币是绑定某个星球的

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetGreenCash(int nValue)
    {
        m_nGreenCash = nValue;
        Main.s_Instance._moneyGreenCash.SetValue ( nValue );

        for (int i = 0; i < m_aryGreenCash.Length; i++ )
        {
            MoneyCounter counter = m_aryGreenCash[i];
            if ( counter == null )
            {
                continue;
            }
            counter.SetValue( nValue );    
        }
    }


    public int GetGreenCash()
    {
        return m_nGreenCash;
    }




} // end class
