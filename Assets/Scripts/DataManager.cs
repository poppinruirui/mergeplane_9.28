﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager : MonoBehaviour {

    public static DataManager s_Instance = null;

    // 交通工具
    Dictionary<string, int> m_dicCoinGainPerRound = new Dictionary<string, int>(); // 每走完一圈获得金币
    Dictionary<string, int> m_dicCoinVehiclePrice = new Dictionary<string, int>(); // 交通工具的售价
    float[] m_aryRoundTimeByLevel = new float[40]; // 交通工具绕一圈的时间


    Dictionary<string, int> m_dicCoinCostToPrestige = new Dictionary<string, int>(); // 每个板块的重生价格

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
        Init();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Init()
    {
        // 交通工具每走一圈所获得的金币
        for (int i = 0; i < 3; i++ ) // planet
        {
            for (int j = 0; j < 5; j++ ) // district
            {
                for (int k = 1; k <= 40;  k++ ) // level
                {
                    int nValue = k  * ( j + 1 ) * ( i + 1 );
                    string szKey = i + "_" + j + "_" + k;
                    m_dicCoinGainPerRound[szKey] = nValue;
                } // end for k
            } // end for j
        } // end for i

        // 交通工具的售价
        for (int i = 0; i < 3; i++) // planet
        {
            for (int j = 0; j < 5; j++) // district
            {
                for (int k = 1; k <= 40; k++) // level
                {
                    int nValue = k * (j + 1) * (i + 1) * 100;
                    string szKey = i + "_" + j + "_" + k;
                    m_dicCoinVehiclePrice[szKey] = nValue;
                } // end for k
            } // end for j
        } // end for i



        // 每一个板块重生所需要的金币
        for (int i = 0; i < 3; i++) // planet
        {
            for (int j = 0; j < 5; j++) // district
            {
                    int nValue = 10000 * (j + 1) * (i + 1);
                    string szKey = i + "_" + j;
                    m_dicCoinCostToPrestige[szKey] = nValue;
            } // end for j
        } // end for i

        for (int i = 0; i < 40; i++)
        {
            m_aryRoundTimeByLevel[i] = 6f - 0.2f * i;
        }
        // poppin to discuss
        // 随着重生次数增加，同一个板块的重生花费应该不同吧？？

        // poppin to discuss 
        // 不同的星球和板块，重生之后的收益数值是否应该不同


    }

    // 获取：交通工具每走一圈所获得的金币
    public int GetCoinGainperRound( int nPlanetId, int nDistrictId, int nLevel )
    {
        string szKey = nPlanetId + "_" + nDistrictId + "_" + nLevel;
        int nValue = 0;
        if ( !m_dicCoinGainPerRound.TryGetValue( szKey, out nValue ) )
        {
            nValue = 0;
        }
        return nValue;
    }

    // 获取：交通工具的售价
    public int GetVehiclePrice(int nPlanetId, int nDistrictId, int nLevel)
    {
        string szKey = nPlanetId + "_" + nDistrictId + "_" + nLevel;
        int nValue = 0;
        if (!m_dicCoinVehiclePrice.TryGetValue(szKey, out nValue))
        {
            nValue = 0;
        }

        // 根据购买次数，价格会增加
        Planet planet = MapManager.s_Instance.GetPlanetById(nPlanetId);
        District district = planet.GetDistrictById(nDistrictId);
        int nBuyTimes = district.GetVehicleBuyTimes(nLevel);

        // 公式是瞎鸡巴乱填的，后期数值策划来弄 
        if (nBuyTimes > 0)
        {
            nValue *= ( nBuyTimes + 1 );
        }


        return nValue;
    }

    public float GetVehicleRunTimePerRound( int nLevel )
    {
        int nIndex = nLevel;
        return m_aryRoundTimeByLevel[nIndex];
    }

    public int GetPrestigaeCoinCost(int nPlanetId, int nDistrictId )
    {
        string szKey = nPlanetId + "_" + nDistrictId;
        int nValue = 0;

        if (!m_dicCoinCostToPrestige.TryGetValue(szKey, out nValue))
        {
            nValue = 0;
        }
        return nValue;
    }
   
    public int GetPrestigeGain(int nPlanetId, int nDistrictId, int nCurPrestigeTimes)
    {
        if (nCurPrestigeTimes == 0)
        {
            return 1;
        }
        return nCurPrestigeTimes * 2;
    }

}
