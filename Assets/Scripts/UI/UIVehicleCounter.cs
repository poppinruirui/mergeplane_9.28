﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIVehicleCounter : MonoBehaviour {

    /// <summary>
    /// UI
    /// </summary>
    public Text _txtLevel;
    public Image _imgAvatar;
    public Text _txtSpeed;
    public Text _txtGain;
    public Text _txtPrice;

    public Text _txtInitialPrice;
    public Text _txtSkillDiscount;
    public Text _txtScienceDiscount;
    // end UI

    /// <summary>
    /// Logic Data
    /// </summary>
    public int m_nVehicleLevel;
    public int m_nPrice;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnClickBuy()
    {
        if (MapManager.s_Instance.GetCurPlanet().GetCoin() < m_nPrice)
        {
            UIMsgBox.s_Instance.ShowMsg( "金币不够" );
            return;
        }

        bool bRet = Main.s_Instance.BuyOneVehicle( m_nVehicleLevel );
        if (bRet)
        {
            UIMsgBox.s_Instance.ShowMsg("购买成功");
            MapManager.s_Instance.GetCurPlanet().SetCoin(MapManager.s_Instance.GetCurPlanet().GetCoin() - m_nPrice);
            MapManager.s_Instance.GetCurDistrict().AddBuyTimes(m_nVehicleLevel);
            UpdatePrice();

            TanGeChe.s_Instance.UpdateCarMallInfo();
        }

    }

    public void UpdatePrice()
    {
        int nPrice = DataManager.s_Instance.GetVehiclePrice(MapManager.s_Instance.GetCurPlanet().GetId(), MapManager.s_Instance.GetCurDistrict().GetId(), m_nVehicleLevel);
        _txtPrice.text = nPrice.ToString();;
        m_nPrice = nPrice;


    }


} // end class
