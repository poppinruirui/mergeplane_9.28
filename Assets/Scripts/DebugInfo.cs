﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class DebugInfo : MonoBehaviour {

    public static DebugInfo s_Instance = null;

    public Text _txtPlanetAndDistrictId;

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetPlanetAndDistrict( int nPlanetId, int nDistrictId )
    {
        _txtPlanetAndDistrictId.text = "星球:" + nPlanetId + "," + "板块:" + nDistrictId;
    }

} // end class
