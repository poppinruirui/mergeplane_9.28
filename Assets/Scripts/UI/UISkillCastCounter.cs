﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UISkillCastCounter : MonoBehaviour {

    /// <summary>
    /// UI
    /// </summary>
    public Text _txtLeftTime;

    public Image _imgColdDownMask;

    // end UI

    public SkillManager.eSkillType m_eType = SkillManager.eSkillType.coin_raise;
    Skill m_BoundSkill = null;

    public BaseScale _baseScale;


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnClick_CastSkill()
    {
        Skill skill = MapManager.s_Instance.GetCurDistrict().GetSkill(m_eType);

        if (skill.GetStatus() != SkillManager.eSkillStatus.idle)
        {
            return;
        }

        BeginCast();
    }




void BeginCast()
{
        _baseScale.BeginScale();

    m_BoundSkill = MapManager.s_Instance.GetCurDistrict().GetSkill(m_eType);
    m_BoundSkill.m_BoundUIBUtton = this;
        m_BoundSkill.SetStartTime(Main.GetSystemTime());
    if (m_BoundSkill.GetSkillType() == SkillManager.eSkillType.speed_accelerate)
    {
        m_BoundSkill.m_fRealTimeDuration = m_BoundSkill.m_Config.nDuration * (1 + ScienceTree.s_Instance.GetSkillSpeedTimeRaise());
    }
    else
    {
        m_BoundSkill.m_fRealTimeDuration = m_BoundSkill.m_Config.nDuration;
    }

    m_BoundSkill.SetStatus(SkillManager.eSkillStatus.working);

   // _txtStatus.text = "使用中...";


    switch (m_eType)
    {
        case SkillManager.eSkillType.coin_raise:
            {
                Main.s_Instance.UpdateRaise();
            }
            break;

        case SkillManager.eSkillType.speed_accelerate:
            {
                float fAccelerateValue = m_BoundSkill.m_Config.fValue;
                Main.s_Instance.AccelerateAll(fAccelerateValue);
            }
            break;
        case SkillManager.eSkillType.cost_reduce:
            {
                TanGeChe.s_Instance.UpdateCarMallInfo();
            }
            break;
    } // end switch



}



} // end class

