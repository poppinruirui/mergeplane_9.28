﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class District : MonoBehaviour {

    public Planet m_BoundPlanet = null;

    public MapManager.eDistrictStatus m_eStatus = MapManager.eDistrictStatus.unlocked;
    public int m_nId = 0;

    public int m_nUnlockPrice = 0; //  解锁该区域所需的金币数量


    string m_szData = "";
    int m_nPrestigeTimes = 0;

    int[] m_aryVehicleBuyTimes = new int[100];

    int m_nAdsRaiseTime = 0;

    // 主动技能的信息(技能是绑定在每个板块上的，每个板块有自己的技能状态)
    Dictionary<SkillManager.eSkillType, Skill> m_dicSkill = new Dictionary<SkillManager.eSkillType, Skill>();

    SkillManager.sSkillConfig config;

    public int m_nAdsBaseTime = 60;

    // Use this for initialization
    void Start () {
        for (int i = 0; i < 100; i++)
        {
            m_aryVehicleBuyTimes[i] = 0;
        }

        for (int i = 0; i < (int)SkillManager.eSkillType.total_num; i++ )
        {
            Skill skill = ResourceManager.s_Instance.NewSkill();
            skill.SetBound(m_BoundPlanet, this );
            SkillManager.eSkillType eType = (SkillManager.eSkillType)i;
            skill.SetType(eType);
            ScienceTree.s_Instance.GetSkillConfig(eType, ref config);
            skill.SetConfig(config);


            m_dicSkill[(SkillManager.eSkillType)i] = skill;


        }

    }

    public Skill GetSkill( SkillManager.eSkillType eType )
    {
        Skill skill = null;
        if ( !m_dicSkill.TryGetValue(eType, out skill) )
        {
            Debug.LogError( "bug!" );
        }
        return skill;
    }
	
	// Update is called once per frame
	void Update () {
        OfflineLoop();

       
    }

    public void SetStatus(MapManager.eDistrictStatus eStatus)
    {
        m_eStatus = eStatus;
    }

    public MapManager.eDistrictStatus GetStatus()
    {
        return m_eStatus;
    }


    public int GetId()
    {
        return m_nId;
    }

    public void SetData( string szData )
    {
        m_szData = szData;
    }

    public string GetData()
    {
        return m_szData;
    }

    public int GetUnlockPrice()
    {
        return m_nUnlockPrice;
    }

    public void DoUnlock()
    {
        SetStatus(MapManager.eDistrictStatus.unlocked);
    }



    public int GetPrestigeTimes()
    {
        return m_nPrestigeTimes;
    }

    public void SetPrestigeTimes(int nValue)
    {
        m_nPrestigeTimes = nValue;
    }

    public void DoPrestige()
    {
       
        SetPrestigeTimes(GetPrestigeTimes() + 1);
        Main.s_Instance.ClearAll();

        UIMsgBox.s_Instance.ShowMsg( "重生成功");

    }

    public int GetVehicleBuyTimes( int nLevel )
    {
        int nIndex = nLevel - 1;
        return m_aryVehicleBuyTimes[nIndex];
    }

    public void AddBuyTimes( int nLevel )
    {
        int nIndex = nLevel - 1;
        m_aryVehicleBuyTimes[nIndex]++;
    }

    public void GainAdsRaise()
    {
        float fAdsTimeRaise = ScienceTree.s_Instance.GetAdsTimeRaise();

        m_nAdsRaiseTime += (int)( m_nAdsBaseTime * ( 1 + fAdsTimeRaise) );

        if (MapManager.s_Instance.GetCurDistrict() == this)
        {
            Main.s_Instance.UpdateRaise();
        }
    }

    public void AdsRaiseTimeLoop()
    {
        if (m_nAdsRaiseTime <= 0)
        {
            return;
        }

        m_nAdsRaiseTime -= 1;

        if (m_nAdsRaiseTime <= 0)
        {
            if (MapManager.s_Instance.GetCurDistrict() == this)
            {
                Main.s_Instance.UpdateRaise();
            }
        }
    }

    public int GetAdsLeftTime()
    {
        return m_nAdsRaiseTime;
    }

    public float GetAdsRaise()
    {
        if (m_nAdsRaiseTime <= 0)
        {
            return 0;
        }
        else
        {
            return AdsManager.s_Instance.GetAdsRaise();
        }
    }

    ///////////////////// 离线收益相关 /////////////////////

    System.DateTime m_fStartOfflineTime;
    public void SetStartOfflineTime(System.DateTime fTime)
    {
        m_fStartOfflineTime = fTime;
    }

    public System.DateTime GetStartOfflineTime()
    {
        return m_fStartOfflineTime;
    }

    //  离线。注意是指这个District离线，未必是整个app离线了。
    float m_fOfflineGainPerSecondBase = 0f;
    float m_fOfflineGainPerSecondReal = 0f;
    bool m_bOffline = true;
    public void OffLine()
    {
        SetStartOfflineTime( Main.GetSystemTime() );

        m_bOffline = true;
        m_fOfflineGainPerSecondBase = 0;

        /*
        // 离线的时候计算一下当前的离线产出率：暂定为当前在线产出率的10%. 
        List<Plane> lstRunnig = Main.s_Instance.GetRunningList();
        for (int i = 0; i < lstRunnig.Count; i++ )
        {
            Plane plane = lstRunnig[i];
            int nGainCointPerRound = plane.GetCoinGainPerRound(); // 每一圈的收益
            float fTimeOfOneRound = Main.s_Instance.GetOneRoundTimeByLevel(plane.GetLevel()); // 每圈需要多少秒（原始值）
            float fRoundPerSecond = 1f / fTimeOfOneRound; // 每秒多少圈
            // 结合当前的技能树加成，得出每圈实际需要多少秒。这里只考虑科技树加成，不考虑主动技能加成，因为离线状态主动技能无效。
            float fScienceSpeedRaise = ScienceTree.s_Instance.GetAccelerateRaise( m_BoundPlanet.GetId() );
            fRoundPerSecond *= ( 1f + fScienceSpeedRaise);

            float fOffLineGainOfthisPlane = fRoundPerSecond * nGainCointPerRound;
            m_fOfflineGainPerSecondBase += fOffLineGainOfthisPlane;
        } // end for

        m_fOfflineGainPerSecondReal = m_fOfflineGainPerSecondBase * Main.s_Instance.UpdateRaise(m_BoundPlanet, this, true);
        */
        CalculateDPS();
        m_fOfflineGainPerSecondReal = m_fDPS;
    }

    float m_fDPS = 0f;
    public float CalculateDPS()
    {
        m_fDPS = 0f;

        List<Plane> lstRunnig = Main.s_Instance.GetRunningList();
        for (int i = 0; i < lstRunnig.Count; i++)
        {
            Plane plane = lstRunnig[i];
            int nGainCointPerRound = plane.GetCoinGainPerRound(); // 每一圈的收益
            float fTimeOfOneRound = Main.s_Instance.GetOneRoundTimeByLevel(plane.GetLevel()); // 每圈需要多少秒（原始值）
            float fRoundPerSecond = 1f / fTimeOfOneRound; // 每秒多少圈
            // 结合当前的技能树加成，得出每圈实际需要多少秒。这里只考虑科技树加成，不考虑主动技能加成，因为离线状态主动技能无效。
            float fScienceSpeedRaise = ScienceTree.s_Instance.GetAccelerateRaise(m_BoundPlanet.GetId());
            fRoundPerSecond *= (1f + fScienceSpeedRaise);

            float fDpsOfthisPlane = fRoundPerSecond * nGainCointPerRound;
            m_fDPS += fDpsOfthisPlane;
        } // end for
        m_fDPS *= Main.s_Instance.UpdateRaise(m_BoundPlanet, this, true);

        return m_fDPS;
    }

    public float GetCurTotalOfflineGain()
    {
        return m_fCurTotalOfflineGain;

    }

    public void SetCurTotalOfflineGain( float fValue )
    {
         m_fCurTotalOfflineGain = fValue;

    }

    //  手机和电脑不一样。手机程序失去焦点之后逻辑是完全不运行的
    public float CalculateOffLineProfit()
    {
        System.TimeSpan fTimeElapse = Main.GetSystemTime() -  GetStartOfflineTime();


    
        float fProfit =  m_fOfflineGainPerSecondReal * fTimeElapse.Seconds;
    
        return fProfit;
    }

    public void OnLine()
    {
        m_bOffline = false;
    }

    // 10秒计算一次收益
    const float OFFLINE_GAIN_INTERVAL = 10f;
    float m_fTimeElapseOffline = 0f;
    float m_fCurTotalOfflineGain = 0f;
    void OfflineLoop()
    {

        // 离线总收入不清零，要领取了才清零

        if ( !m_bOffline)
        {
            return;
        }

        m_fTimeElapseOffline += Time.deltaTime;
        if (m_fTimeElapseOffline < OFFLINE_GAIN_INTERVAL)
        {
            return;
        }
        m_fTimeElapseOffline = 0; 

        float fOfflineGain = m_fOfflineGainPerSecondReal * OFFLINE_GAIN_INTERVAL;
        if (fOfflineGain > 10f)
        {
            //Debug.Log( "district[" + GetId() + "]离线收入增加：" + fOfflineGain);
            m_fCurTotalOfflineGain += fOfflineGain;
        }
    }

    /// end 离线收益相关




} // end class
