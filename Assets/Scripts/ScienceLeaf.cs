﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScienceLeaf : MonoBehaviour {

    public ScienceTree.eBranchType m_eType = ScienceTree.eBranchType.branch0;
    public int m_nIndexOfThisBranch = 0;

    public int m_nLevel =0;

    public ScienceTreeConfig m_Config = null;

    private void Awake()
    {
       
    }

    // Use this for initialization
    void Start () {
       

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnClickMe()
    {
        ScienceTree.s_Instance.ProcessLeaf( this );
    }

    public float GetCurValue()
    {
        if ( m_Config == null )
        {
            return 0;
        }
        return m_Config.GetValue(m_nLevel);
    }

    public float GetNextValue()
    {
        return m_Config.GetValue(m_nLevel + 1);
    }

    public int GetNextPrice()
    {
        return m_Config.GetPrice(m_nLevel + 1);
    }

    public void SetLevel( int nLevel )
    {
        m_nLevel = nLevel;
    }

    public int GetLevel()
    {
        return m_nLevel;
    }

    public bool IsUnlocked()
    {
        return m_nLevel > 0;
    }

    public string GetDesc()
    {
        return m_Config.szDesc;
    }

    public ScienceTree.eScienceType GetScienceType()
    {
        return m_Config.m_eScienceType;
    }



} // end class
