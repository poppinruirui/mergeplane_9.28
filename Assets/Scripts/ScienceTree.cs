﻿/*
 * 科技树
 * 
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScienceTree : MonoBehaviour {

    public static ScienceTree s_Instance = null;

    public const int MAX_LEAF_NUM_OF_BRANCH = 30;
    public const int MAX_LEVEL_OF_LEAF = 30;

    public enum eBranchType
    {
        branch0,
        branch1,
        branch2,
    };

    public enum eScienceType
    {
        coin_raise,
        cost_reduce,
        ads_duration,
        ads_coin_raise,
        speed_accelerate,

        skill_speed_accelerate_time_raise,
        skill_coin_raise_raise,
    };

    Dictionary<eBranchType, int> m_dicSkillPoint = new Dictionary<eBranchType, int>();

    public ScienceLeaf[] aryBranch0;
    public ScienceLeaf[] aryBranch1;
    public ScienceLeaf[] aryBranch2;

    /// <summary>
    /// UI
    /// </summary>
    public GameObject _panelScienceTree;

    // sub-panel 
    public GameObject _subpanelUpgrade;
    public Text _txtLevel;
    public GameObject _containerThisLevelInfo;
    public GameObject _containerNextLevelInfo;

    public Button _btnUpgrade;
    public Text _txtPrice;
    public Image _imgMoneyIcon;
    public Text _txtOperateType;

    public GameObject _containerLevel;

    public Text _txtCurLevelValue;
    public Text _txtNextLevelValue;
    public Text _txtDesc;

    public MoneyCounter _moneyBranch0;
    public MoneyCounter _moneyBranch1;
    public MoneyCounter _moneyBranch2;

    // end UI

    Dictionary<eBranchType, ScienceLeaf[]> m_dicTree = new Dictionary<eBranchType, ScienceLeaf[]>();

    public ScienceLeaf m_CurSelectedLeaf = null;

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
        LoadConfig();

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void GetSkillConfig( SkillManager.eSkillType eType, ref SkillManager.sSkillConfig config )
    {
        switch(eType)
        {
            case SkillManager.eSkillType.speed_accelerate:
                {
                    config.fValue = 3f;
                    config.nColdDown = 10;
                    config.nDuration = 30;
                }
                break;
            case SkillManager.eSkillType.coin_raise:
                {
                    config.fValue = 2f;
                    config.nColdDown = 10;
                    config.nDuration = 30;
                }
                break;
            case SkillManager.eSkillType.cost_reduce:
                {
                    config.fValue = -0.4f;
                    config.nColdDown = 10;
                    config.nDuration = 30;
                }
                break;
        } // end switch

    }

    public void OpenScienceTree()
    {
        _panelScienceTree.SetActive( true );
    }

    public void CloseScienceTree()
    {
        _panelScienceTree.SetActive(false);
    }

    public void OpenSubPanelUpgrade()
    {
        _subpanelUpgrade.SetActive( true );
    }

    public void CloseSubPanelUpgrade()
    {
        _subpanelUpgrade.SetActive(false);
    }

    public void LoadConfig()
    {
        // poppin test 这里写死一些数据。正式版是读配置文件
        //// branch 0



        // leaf 0
        for (int i = 0; i < 5; i++ )
        {
            if ( i >= aryBranch0.Length)
            {
                break;
            }

            ScienceLeaf leaf = aryBranch0[i];

            if ( leaf == null )
            {
                continue;
            }

            ScienceTreeConfig config = ResourceManager.s_Instance.NewTreeConfig();

            config.szDesc = "星球0" + "板块" + i + "金币增益";
            config.m_szKey = "0_" + i;
            config.m_eScienceType = eScienceType.coin_raise;

            for (int j = 0; j < MAX_LEVEL_OF_LEAF; j++)
            {
                int nLevel = j + 1;
                config.aryPrice[j] = nLevel;
                config.aryValue[j] = nLevel + 1;
            } // end for

            leaf.m_Config = config;
            aryBranch0[i] = leaf;

            // end leaf 0
        } // end for i



        //// end branch 0


        //// ------------------- branch 1 -----------------
        int nPlanetIndex = 0;
        for (int i = 0; i < 5; i += 2)
        {
            ScienceLeaf leaf = aryBranch1[i];

            if (leaf == null)
            {
                continue;
            }

            ScienceTreeConfig config = ResourceManager.s_Instance.NewTreeConfig();

            config.szDesc = "星球" + nPlanetIndex + "交通工具打折";
            config.m_szKey = nPlanetIndex.ToString();
            config.m_eScienceType = eScienceType.cost_reduce;

            for (int j = 0; j < MAX_LEVEL_OF_LEAF; j++)
            {
                int nLevel = j + 1;
                config.aryPrice[j] = nLevel;
                config.aryValue[j] = -0.3f - j * 0.02f;
                if (config.aryValue[j] < -0.8f)
                {
                    config.aryValue[j] = -0.8f;
                }
            } // end for

            leaf.m_Config = config;
            aryBranch1[i] = leaf;


            nPlanetIndex++;
        } // end for i

        // 广告持续时间
        ScienceLeaf leaf_1_1 = aryBranch1[1];
        ScienceTreeConfig config_1_1 = ResourceManager.s_Instance.NewTreeConfig();
        config_1_1.szDesc = "广告提升持续时间提高";
        for (int j = 0; j < MAX_LEVEL_OF_LEAF; j++)
        {
            int nLevel = j + 1;
            config_1_1.aryPrice[j] = nLevel;
            config_1_1.aryValue[j] = 0.15f * nLevel;
           
        } // end for j
        leaf_1_1.m_Config = config_1_1;

        // end 广告持续时间

        // 广告金币加成
        ScienceLeaf leaf_1_3 = aryBranch1[3];
        ScienceTreeConfig config_1_3 = ResourceManager.s_Instance.NewTreeConfig();
        config_1_3.szDesc = "广告金币加成提高";
        for (int j = 0; j < MAX_LEVEL_OF_LEAF; j++)
        {
            int nLevel = j + 1;
            config_1_3.aryPrice[j] = nLevel;
            config_1_3.aryValue[j] = 0.2f * nLevel;

        } // end for j
        leaf_1_3.m_Config = config_1_3;

        // end 广告金币加成


        /// ----------------- end branch 1 ---------------


        //// ------------------ branch 2 ---------------------
        nPlanetIndex = 0;
        for (int i = 0; i < 5; i += 2)
        {
            ScienceLeaf leaf = aryBranch2[i];

            if (leaf == null)
            {
                continue;
            }

            ScienceTreeConfig config = ResourceManager.s_Instance.NewTreeConfig();

            config.szDesc = "星球" + nPlanetIndex + "交通工具加速";
            config.m_szKey = nPlanetIndex.ToString();
            config.m_eScienceType = eScienceType.speed_accelerate;

            for (int j = 0; j < MAX_LEVEL_OF_LEAF; j++)
            {
                int nLevel = j + 1;
                config.aryPrice[j] = nLevel;
                config.aryValue[j] = 0.2f * nLevel;
               
            } // end for

            leaf.m_Config = config;


            nPlanetIndex++;
        } // end for i

        ScienceLeaf leaf_2_1 = aryBranch2[1];
        ScienceTreeConfig config_2_1 = ResourceManager.s_Instance.NewTreeConfig();
        config_2_1.szDesc = "加速技能持续时间提高";
        config_2_1.m_eScienceType = eScienceType.skill_speed_accelerate_time_raise;
        for (int j = 0; j < MAX_LEVEL_OF_LEAF; j++)
        {
            int nLevel = j + 1;
            config_2_1.aryPrice[j] = nLevel;
            config_2_1.aryValue[j] = 0.2f * nLevel;

        } // end for

        leaf_2_1.m_Config = config_2_1;
        // end 


        ScienceLeaf leaf_2_3 = aryBranch2[3];
        ScienceTreeConfig config_2_3 = ResourceManager.s_Instance.NewTreeConfig();
        config_2_3.szDesc = "金币增益技能提高";
        config_2_3.m_eScienceType = eScienceType.skill_coin_raise_raise;
        for (int j = 0; j < MAX_LEVEL_OF_LEAF; j++)
        {
            int nLevel = j + 1;
            config_2_3.aryPrice[j] = nLevel;
            config_2_3.aryValue[j] = 0.2f * nLevel;

        } // end for

        leaf_2_3.m_Config = config_2_3;

        /// ----------- end branch 2 --------------


        m_dicTree[eBranchType.branch0] = aryBranch0;
        m_dicTree[eBranchType.branch1] = aryBranch1;
        m_dicTree[eBranchType.branch2] = aryBranch2;
        //// branch 1

    }

    public float GetSkillCoinRaiseRaise()
    {
        return aryBranch2[3].GetCurValue();
    }

    public float GetSkillSpeedTimeRaise()
    {
        return aryBranch2[1].GetCurValue();
    }

    public float GetAccelerateRaise( int nPlanetId )
    {
        switch(nPlanetId)
        {
            case 0:
                {
                    return aryBranch2[0].GetCurValue();
                }
                break;
            case 1:
                {
                    return aryBranch2[2].GetCurValue();
                }
                break;
            case 2:
                {
                    return aryBranch2[4].GetCurValue();
                }
                break;
        } // end switch

        return 0;
    }

    // 获取广告加成值
    public float GetAdsCoinRaise()
    {

        ScienceLeaf leaf = aryBranch1[3];
        return leaf.GetCurValue();
    }

    // 获取广告时间提升值
    public float GetAdsTimeRaise()
    {

        ScienceLeaf leaf = aryBranch1[1];
        return leaf.GetCurValue();
    }


    //  获取交通工具购买折扣
    public float GetVehicleBuyDiscount(int nPlanetId )
    {
        float fDiscount = 0f;
        string szKey = nPlanetId.ToString();
        ScienceLeaf[] aryBranch1 = m_dicTree[eBranchType.branch1];
        for (int i = 0; i < aryBranch1.Length; i++)
        {
            ScienceLeaf leaf = aryBranch1[i];
            if (leaf == null)
            {
                return 0;
            }
            if (leaf.m_Config.m_szKey == szKey)
            {
                return leaf.GetCurValue();
            }
        }

        return fDiscount;
    }

    //  获取金币加成的数值
    public float GetCoinRaise( int nPlanetId, int nDistrictId )
    {
        float fRaise = 0f;

        string szKey = nPlanetId + "_" + nDistrictId;
        ScienceLeaf[] aryBranch0 = m_dicTree[eBranchType.branch0];
        for (int i = 0; i < aryBranch0.Length; i++ )
        {
            ScienceLeaf leaf = aryBranch0[i];
            if ( leaf == null )
            {
                return 0;
            }
            if ( leaf.m_Config.m_szKey == szKey )
            {
                return leaf.GetCurValue();
            }
        }

        return fRaise;
    }

    public void SetValueUiContent( eScienceType eType, Text txtValue, float fValue )
    {
        switch(eType)
        {
            case eScienceType.cost_reduce:
                {
                    txtValue.text = ( fValue * 100 )+ "%";
                }
                break;

            default:
                {
                    txtValue.text = fValue.ToString();
                }
                break;
        } // end switch

    }

    public void UpdateSubPanelInfo(ScienceLeaf leaf)
    {
        if ( leaf.m_Config == null )
        {
            return;
        }

        if (leaf.IsUnlocked()) // 该节点已解锁
        {
            _containerLevel.SetActive(true);
            _txtLevel.text = leaf.GetLevel().ToString();

            _txtOperateType.gameObject.SetActive(true);
            _txtOperateType.text = "升级";

            _containerNextLevelInfo.SetActive( true );

            _btnUpgrade.gameObject.SetActive(true);

            //_txtCurLevelValue.text = leaf.GetCurValue().ToString();
            SetValueUiContent(leaf.GetScienceType(), _txtCurLevelValue, leaf.GetCurValue());
            //_txtNextLevelValue.text = leaf.GetNextValue().ToString();
            SetValueUiContent(leaf.GetScienceType(), _txtNextLevelValue, leaf.GetNextValue());
        }
        else // 该节点未解锁
        {
            bool bCanUnlock = false;
            // 判断该节点能否解锁：如果它的上一个节点还没解锁，则它不能解锁
            if (leaf.m_nIndexOfThisBranch == 0) // 它是这条支线的第一个节点，可以解锁
            {
                bCanUnlock = true;
            }
            else
            {
                // 取它的上一个节点，看看解锁没有
                ScienceLeaf[] branch = m_dicTree[leaf.m_eType];
                int nIndexPrevLeaf = leaf.m_nIndexOfThisBranch - 1;
                ScienceLeaf prev_leaf = branch[nIndexPrevLeaf];
                if (prev_leaf.IsUnlocked()) // 它的上一个节点已解锁
                {
                    bCanUnlock = true;
                }
            }

            _containerLevel.gameObject.SetActive(false);
            _containerNextLevelInfo.SetActive(false);
            if (bCanUnlock) // 可以解锁
            {
                _txtOperateType.text = "解锁";
                _txtOperateType.gameObject.SetActive( true );
                _btnUpgrade.gameObject.SetActive(true);
            }
            else // 不能解锁
            {
                _txtOperateType.gameObject.SetActive(false);
                _btnUpgrade.gameObject.SetActive( false );
            }



            // _txtCurLevelValue.text = leaf.GetNextValue().ToString();
            SetValueUiContent(leaf.GetScienceType(), _txtCurLevelValue, leaf.GetNextValue());


        } // end 该节点未解锁

        _txtDesc.text = leaf.GetDesc();

        int nPrice = leaf.GetNextPrice();

        _txtPrice.text = nPrice.ToString();

    }

    public void ProcessLeaf( ScienceLeaf leaf )
    {
        m_CurSelectedLeaf = leaf;

        _subpanelUpgrade.SetActive( true );


        UpdateSubPanelInfo(m_CurSelectedLeaf);
    }

    public void OnClick_UnlockOrUpgrade()
    {
        eBranchType eType = m_CurSelectedLeaf.m_eType;
        int nPoint = GetSkillPoint(eType);
        int nToCost = m_CurSelectedLeaf.GetNextPrice();

        if ( nPoint < nToCost)
        {
            UIMsgBox.s_Instance.ShowMsg("技能点不够");
            return;
        }

        SetSkillPoint(eType, GetSkillPoint(eType) - nToCost);

        m_CurSelectedLeaf.SetLevel(m_CurSelectedLeaf.GetLevel() + 1);

        UpdateSubPanelInfo(m_CurSelectedLeaf);

        UIMsgBox.s_Instance.ShowMsg("升级成功");

        switch( m_CurSelectedLeaf.GetScienceType())
        {
            case eScienceType.coin_raise:
                {
                    Main.s_Instance.UpdateRaise();
                }
                break;  
            case eScienceType.cost_reduce:
                {
                    TanGeChe.s_Instance.UpdateCarMallInfo();
                }
                break;
        } // end switch


    }

    public int GetSkillPoint( eBranchType eType )
    {
        int nPoint = 0;

        if ( !m_dicSkillPoint.TryGetValue( eType, out nPoint ) )
        {
            return 0;
        }

        return nPoint;
    }

    public void SetSkillPoint(eBranchType eType, int nPoint)
    {
        m_dicSkillPoint[eType] = nPoint;

        switch( eType )
        {
            case eBranchType.branch0:
                {
                    _moneyBranch0.SetValue(nPoint);
                }
                break;
                
            case eBranchType.branch1:
                {
                    _moneyBranch1.SetValue(nPoint);
                }
                break;
            case eBranchType.branch2:
                {
                    _moneyBranch2.SetValue(nPoint);
                }
                break;
        } // end switch
    }


}// end class
