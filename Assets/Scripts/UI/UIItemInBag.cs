﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIItemInBag : MonoBehaviour {

    public Text _txtFuncValue;
    public Text _txtLeftTime;

    public int m_nItemId = 0;
    public int m_nValue0 = 0;
    public int m_nDuration = 0;

    public System.DateTime m_StartTime;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void InitBagItem(UIItem buy_item)
    {
        m_nItemId = buy_item.m_nItemId;
        m_nValue0 = buy_item.m_nValue0;
        m_nDuration = buy_item.m_nDuration;

        _txtFuncValue.text = "X" + m_nValue0;
        _txtLeftTime.text = m_nDuration + "S";
    }

    public void OnClickButton_Use()
    {
         ItemSystem.s_Instance.UseItem( this );
    }


    public bool DoCount()
    {
        System.TimeSpan fTimeElapse = Main.GetSystemTime() - m_StartTime;
        float fTimeLeft = m_nDuration - fTimeElapse.Seconds;
        _txtLeftTime.text = fTimeLeft.ToString("f0");

        if (fTimeLeft <= 0)
        {
            return true;
        }

        return false;
    }

    public void InitBagItemById(int nItemId)
    {
  
        m_nItemId = nItemId;

        int nValue0 = 0;
        int nDuration = 0;
        string szDesc = "";
        string szName = "";
        switch (nItemId)
        {
            case 1:
                {
                    nValue0 = 2;
                    nDuration = 30;
                    szDesc = "30秒";
                    szName = "2倍";
                }
                break;
        } // end switch


        m_nValue0 = nValue0;
        m_nDuration = nDuration;
        _txtFuncValue.text = "X" + m_nValue0;
        _txtLeftTime.text = m_nDuration + "S";
    }





} // end class
