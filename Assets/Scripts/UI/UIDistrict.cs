﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIDistrict : MonoBehaviour {

    public GameObject _lock;
    public bool m_bLocked = true;
    public bool m_bCanUnlock = false;

    public District m_District;

    public GameObject _goWenHao;

    public Text _txtUnlockPrice ;
    public GameObject _goPrice;

    public Button _btnEnter;

    public float m_fEnterButtonShowTime = 0;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        ShowButtonEnterLoop();

    }

    public void SetDistrict( District district )
    {
        m_District = district;
        SetUnlockPrice( district.GetUnlockPrice() );
    }

    public void SetLocked( bool bLocked )
    {
        _lock.SetActive( bLocked);
        _goPrice.SetActive(bLocked);
        _txtUnlockPrice.gameObject.SetActive(bLocked);
    }

    public void OnClickMe()
    {
        if ( m_bLocked )
        {
            if (m_bCanUnlock)
            {
                MapManager.s_Instance.PreUnLockDistrict(m_District);
            }
            else
            {
                UIMsgBox.s_Instance.ShowMsg( "上一级板块还没解锁，不能解锁此板块" );
            }
        }
        else
        {
            BeginShowButtonEnter();
        }
    }

    void BeginShowButtonEnter()
    {
        m_fEnterButtonShowTime = 2f;
        _btnEnter.gameObject.SetActive(true);
    }

    void ShowButtonEnterLoop()
    {
        if (m_fEnterButtonShowTime <= 0)
        {
            return;
        }

        m_fEnterButtonShowTime -= Time.deltaTime;
        if (m_fEnterButtonShowTime <= 0)
        {
            _btnEnter.gameObject.SetActive( false );
        }
    }

    public void SetCanUnlock( bool bCan )
    {

        m_bCanUnlock = bCan;
    }

    public void SetUnlockPrice( int nPrice )
    {
        _txtUnlockPrice.text = nPrice.ToString();
    }

    public void OnClickEnterRace()
    {
     //   Debug.Log( "enter: " + MapManager.s_Instance.m_nCurShowPlanetDetialIdOnUI+ "," + m_District.GetId() );
        MapManager.s_Instance.PreEnterRace(MapManager.s_Instance.m_nCurShowPlanetDetialIdOnUI, m_District.GetId());
    }

} // end class
