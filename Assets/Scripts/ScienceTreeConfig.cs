﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScienceTreeConfig : MonoBehaviour {


    public float[] aryValue = new float[ScienceTree.MAX_LEVEL_OF_LEAF];
    public int[] aryPrice = new int[ScienceTree.MAX_LEVEL_OF_LEAF];

    public string szDesc;

    public string m_szKey = "";

    public ScienceLeaf m_CurSelecedLeaf = null;

    public ScienceTree.eScienceType m_eScienceType = ScienceTree.eScienceType.coin_raise;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public float GetValue( int nLevel )
    {
        if ( nLevel == 0 )
        {
            return 0f;
        }

        int nIndex = nLevel - 1;

        if ( nIndex < 0 || nIndex >= aryValue.Length )
        {
            return 0f;
        }

        return aryValue[nIndex];
    }

    public int GetPrice( int nLevel)
    {
        if (nLevel == 0)
        {
            return 0;
        }

        int nIndex = nLevel - 1;

        if (nIndex < 0 || nIndex >= aryValue.Length)
        {
            return 0;
        }

        return aryPrice[nIndex];
    }

} // end class
