﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIItem : MonoBehaviour {

    public bool m_bInBag = false;


    public GameObject _panelCounting;

    public Text _txtName;
    public Text _txtDesc;
    public Text _txtValue;

    public Text _txtLeftTime;
    public Button _btnUse;

    public ShoppinMall.eItemType m_eItemType;
    public ShoppinMall.ePriceType m_ePriceType;
    public int m_nItemId = 0;
    public int m_nPrice = 0;
    public int m_nValue0 = 0;
    public int m_nDuration = 0;

    public float m_fStartTime = 0;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnClickButton_DoSomething()
    {
        if ( m_bInBag )
        {
            OnClickButton_Use();
        }
        else
        {
            OnClickButton_Buy();
        }
    }

    public void OnClickButton_Buy()
    {
        if (m_eItemType == ShoppinMall.eItemType.diamond)
        {
            UIMsgBox.s_Instance.ShowMsg("此功能暂未开放");
            return;
        }

        int nCurHave = 0;
        switch(m_ePriceType)
        {
            case ShoppinMall.ePriceType.green_cash:
                {
                    nCurHave = AccountSystem.s_Instance.GetGreenCash();
                }
                break;

            
        } // end switch

        if (nCurHave < m_nPrice)
        {
            UIMsgBox.s_Instance.ShowMsg( "钱不够" );
            return;
        }

        ShoppinMall.s_Instance._subpanelConfirmBuy.SetActive( true );
        ShoppinMall.s_Instance._CurProcessingBuyUiItem = this;

        string szDesc = "";
       ShoppinMall.s_Instance._txtConfirmBuy_Cost.text = "花费 现金 <Color=#F9E15A>" + ShoppinMall.s_Instance._CurProcessingBuyUiItem.m_nPrice + "</Color> 购买？";
        ShoppinMall.s_Instance._txtConfirmBuy_Value.text = "X" + ShoppinMall.s_Instance._CurProcessingBuyUiItem.m_nValue0;
        if (m_eItemType == ShoppinMall.eItemType.coin_raise)
        {
            ShoppinMall.s_Instance._imgConfirmBuy_Avatar.sprite = ResourceManager.s_Instance.m_sprArrow;
            szDesc = "所有星球在" + ShoppinMall.s_Instance._CurProcessingBuyUiItem.m_nDuration + "秒内收入X" + ShoppinMall.s_Instance._CurProcessingBuyUiItem.m_nValue0;
        }
        else if (m_eItemType == ShoppinMall.eItemType.skill_point)
        {
            ShoppinMall.s_Instance._imgConfirmBuy_Avatar.sprite = ResourceManager.s_Instance.m_arySkillPointIcon[m_nItemId];
            szDesc = "获得" + (m_nItemId) + "号星球的技能点";
        }
        ShoppinMall.s_Instance._txtConfirmBuy_FuncAndValue.text = szDesc;

    }

    public void InitBagItem( UIItem buy_item )
    {
        m_bInBag = true;
        m_nItemId = buy_item.m_nItemId;
        m_nValue0 = buy_item.m_nValue0;
        m_nDuration = buy_item.m_nDuration;
        _txtDesc.text = buy_item._txtDesc.text;
        _txtValue.text = "使用";
        _txtName.text = buy_item._txtName.text;

        SetCountingPanelVisible( false );
        SetUseButtonVisible( true );
    }



    public void InitBagItemById( int nItemId )
    {
        m_bInBag = true;
        m_nItemId = nItemId;

        int nValue0 = 0;
        int nDuration = 0;
        string szDesc = "";
        string szName = "";
        switch( nItemId )
        {
            case 1:
                {
                    nValue0 = 2;
                    nDuration = 30;
                    szDesc = "30秒";
                    szName = "2倍";
                }
                break;
        } // end switch


        m_nValue0 = nValue0;
        m_nDuration = nDuration;
        _txtDesc.text = szDesc;
        _txtValue.text = "使用";
        _txtName.text = szName;

        SetCountingPanelVisible(false);
        SetUseButtonVisible(true);
    }

    public void OnClickButton_Use()
    {
      //  ItemSystem.s_Instance.UseItem( this );
    }

    public void SetCountingPanelVisible( bool bVisible )
    {
      // _panelCounting.SetActive(bVisible);
    }

    public void SetUseButtonVisible( bool bVisible )
    {
       // _btnUse.gameObject.SetActive( bVisible );
    }

    public bool DoCount()
    {
        float fTimeElapse = Main.GetTime() - m_fStartTime;
        float fTimeLeft = m_nDuration - fTimeElapse;
        _txtLeftTime.text = fTimeLeft.ToString( "f0" );

        if (fTimeLeft <= 0)
        {
            return true;
        }

        return false;
    }

} // end class
