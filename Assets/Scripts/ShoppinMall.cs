﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShoppinMall : MonoBehaviour {



    public static ShoppinMall s_Instance = null;

    public GameObject _panelShoppingMall;
    public GameObject _subpanelConfirmBuy;
    public GameObject _subpanelConfirmUse;

    public UIItem _CurProcessingBuyUiItem = null;
    public UIItemInBag _CurProcessingUsingUiItem = null;

    /// <summary>
    /// confirm buy
    /// </summary>
    public Text _txtConfirmBuy_FuncAndValue;
    public Text _txtConfirmBuy_Cost;
    public Text _txtConfirmBuy_Value;
    public Image _imgConfirmBuy_Avatar;

    // 商城道具类型
    public enum eItemType
    {
        coin_raise,  // 金币加成
        diamond,     // 钻石 
        skill_point, // 技能点
    };

    // 商城货币类型
    public enum ePriceType
    {
        green_cash,  // 绿票
    };

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
        Init();

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Init()
    {



    }

    public void OnClick_OpenShoppinMall()
    {
        _panelShoppingMall.SetActive( true );
    }


    public void OnClick_CloseShoppinMall()
    {
        _panelShoppingMall.SetActive(false);
    }

    public void OnClick_OpenSubPanelConfirmBuy()
    {
        _subpanelConfirmBuy.SetActive( true );

    }

    public void OnClick_CloseSubPanelConfirmBuy()
    {
        _subpanelConfirmBuy.SetActive(false);
    }

    public void OnClick_ConfirmBuy()
    {
        if (_CurProcessingBuyUiItem.m_eItemType == eItemType.diamond)
        {
            UIMsgBox.s_Instance.ShowMsg( "此功能暂未开放" );
            return;
        }

        _subpanelConfirmBuy.SetActive(false);
        // 消耗货币
        switch (_CurProcessingBuyUiItem.m_ePriceType)
        {
            case ePriceType.green_cash:
                {
                    AccountSystem.s_Instance.SetGreenCash(AccountSystem.s_Instance.GetGreenCash() - _CurProcessingBuyUiItem.m_nPrice);
                }
                break;
        } // end switch


        if (_CurProcessingBuyUiItem.m_eItemType == eItemType.skill_point)
        {
            UIMsgBox.s_Instance.ShowMsg( "购买技能点成功" );
            ScienceTree.eBranchType eType = (ScienceTree.eBranchType)_CurProcessingBuyUiItem.m_nItemId;
            int nCurPoint = ScienceTree.s_Instance.GetSkillPoint(eType);
            ScienceTree.s_Instance.SetSkillPoint(eType, nCurPoint + 1);
            return;
        }

        _subpanelConfirmUse.SetActive( true );

   

        // 生成一个背包物品
        /*
        UIItem bag_item = ResourceManager.s_Instance.NewUiItem();
        bag_item.InitBagItem( _CurProcessingBuyUiItem );
        _CurProcessingUsingUiItem = bag_item;

        ItemSystem.s_Instance.AddItem(bag_item);
        */
        UIItemInBag bag_item = ResourceManager.s_Instance.NewUiItem();
        bag_item.InitBagItem(_CurProcessingBuyUiItem);
        _CurProcessingUsingUiItem = bag_item;

        ItemSystem.s_Instance.AddItem(bag_item);
    }

    public void OnClick_CloseUsingImmediatelyPanel()
    {
        _subpanelConfirmUse.SetActive(false);
    }

    public void OnClick_UseImmediately()
    {
        ItemSystem.s_Instance.UseItem( _CurProcessingUsingUiItem );
        _subpanelConfirmUse.SetActive( false );

    }

}
