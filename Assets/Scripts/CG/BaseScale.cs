﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseScale : MonoBehaviour {

    static Vector3 vecTempScale = new Vector3();

    public float m_fScaleTime = 0.2f;
    public float m_fWaitTime = 0.2f;
    public float m_fMaxScale = 1.5f;

    int m_nStatus = 0; // 0 - none  1 - Change  2 - wait  3 - back  

    float m_fSpeed = 0f;
    float m_fWaitTimeElapse = 0f;

    public float m_fInitScale = 0f;
    float m_fDestScale = 0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void FixedUpdate()
    {
        Scaling();
    }

    public void BeginScale()
    {
        
        m_nStatus = 1;
        m_fWaitTimeElapse = 0f;

   
        m_fDestScale = m_fInitScale * m_fMaxScale;
        m_fSpeed = ( m_fDestScale - m_fInitScale ) / m_fScaleTime * Time.fixedDeltaTime;

        vecTempScale.x = m_fInitScale;
        vecTempScale.y = m_fInitScale;
        vecTempScale.z = 1f;
        this.transform.localScale = vecTempScale;
    }

    void Scaling()
    {

        if (m_nStatus == 0)
        {
            return;
        }

        if (m_nStatus == 1)
        {
            vecTempScale = this.transform.localScale;
            vecTempScale.x += m_fSpeed;
            vecTempScale.y += m_fSpeed;
            vecTempScale.z = 1f;
            this.transform.localScale = vecTempScale;
            if (vecTempScale.x >= m_fDestScale)
            {
                m_nStatus = 2;
            }
        }
        else if (m_nStatus == 2)
        {
            m_fWaitTimeElapse += Time.fixedDeltaTime;
            if (m_fWaitTimeElapse >= m_fWaitTime)
            {
                m_nStatus = 3;
            }
        }
        else if (m_nStatus == 3)
        {

            vecTempScale = this.transform.localScale;
            vecTempScale.x -= m_fSpeed;
            vecTempScale.y -= m_fSpeed;
            vecTempScale.z = 1f;

            if (vecTempScale.x <= m_fInitScale )
            {
                vecTempScale.x = m_fInitScale;
                vecTempScale.y = m_fInitScale;
                m_nStatus = 0;
            }

            this.transform.localScale = vecTempScale;
        }

    }

 
}
